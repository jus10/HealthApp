import { Login, Auth, Register, Scopes } from "./login";
import { Fitness } from "./fitness";
import { Home } from "./Home";

import { Route, Routes } from "react-router-dom";

const HealthRoutes = () => {
    return (
        <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<Login />} />
            <Route path="/dev" element={<Auth />} />
            <Route path="/register" element={<Register />} />
            <Route path="/scopes" element={<Scopes />} />
            <Route path="/fitness" element={<Fitness />} />
        </Routes>
    );
};

export default HealthRoutes;
