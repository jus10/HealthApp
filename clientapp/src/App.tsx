import HealthRoutes from "./HealthRoutes";
import { NavigationBar } from "./common";

import { ApplicationState } from "./store";
import { Store } from "redux";
import { Provider } from "react-redux";

import "./css/App.css";

interface Props {
    store: Store<ApplicationState>;
}

export const App = (props: Props) => {
    return (
        <Provider store={props.store}>
            <NavigationBar />
            <HealthRoutes />
        </Provider>
    );
};
