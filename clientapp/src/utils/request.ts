export const requestHelper = {
    authorize: async function(scope: string): Promise<Response>{
        const res = await fetch(`/auth/authorize?scope=${scope}`, {
            method: "GET",
            mode: "cors",
            headers: {
                "Access-Control-Allow-Origin": "*"
            }
        });

        return res;
    },

    getBodyWeight: async (startDate: Date, endDate: Date) => {},

    login: async function(email: string, password: string): Promise<Response>{
        const res = await fetch("/auth/login", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        });

        return res;
    },

    register: async function(
        firstName: string,
        lastName: string,
        password: string,
        dob: Date,
        email: string
    ): Promise<Response> {
        const res = await fetch("/auth/register", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                firstname: firstName,
                lastname: lastName,
                password: password,
                email: email,
                dateofbirth: dob
            })
        });

        return res;
    }
};
