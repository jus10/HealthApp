export function transformTableData(data: Map<string, {}>): Array<Map<string, {}>> {
    if (data !== null && Object.keys(data).length !== 0) {
        let tableRowId = 1; // React Table Key ID starts at 1
        let values = new Array<any>();
        for (let val of Object.values(data)) {
            let obj: any = {};
            obj["tableRowId"] = tableRowId++;
            for (let nestedKey of Object.keys(val)) {
                obj[nestedKey] = val[nestedKey];
            }
            values.push(obj);
        }

        return values;
    }
    // Return empty on first render
    return new Array<Map<string, {}>>();
}
