import { createSlice } from "@reduxjs/toolkit";
import { ApplicationState } from "..";

const initialState: ApplicationState = {
    isLoggedIn: false
};

export const userSlice = createSlice({
    name: "user",
    initialState: initialState,
    reducers: {
        authenticate: (state) => {
            state.isLoggedIn = true;
        },
        unauthenticate: (state) => {
            state.isLoggedIn = false;
        }
    }
});

export const { authenticate, unauthenticate } = userSlice.actions;
