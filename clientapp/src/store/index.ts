import { userSlice } from "./user";
import { configureStore } from "@reduxjs/toolkit";

export interface ApplicationState {
    isLoggedIn: boolean;
}

export const store = configureStore({
    reducer: userSlice.reducer
});
