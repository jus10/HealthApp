const { createProxyMiddleware } = require("http-proxy-middleware");

module.exports = function (app) {
    app.use(
        "/api/v1/health",
        createProxyMiddleware({
            target: "https://localhost:5001",
            changeOrigin: true,
            secure: false
        })
    );
    app.use(
        "/auth",
        createProxyMiddleware({
            target: "https://localhost:5001",
            changeOrigin: true,
            secure: false
        })
    );
    app.use(
        "/api/v1/fitness",
        createProxyMiddleware({
            target: "https://localhost:5001",
            changeOrigin: true,
            secure: false
        })
    );
};
