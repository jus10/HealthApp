import { requestHelper } from "../utils/request";

import React, { useEffect, useState } from "react";

import "../css/RegisterForm.css";
import { authenticate } from "../store/user";
import { store } from "../store";
import { useNavigate } from "react-router-dom";

export function Register() {
    const nav = useNavigate();

    async function handleSubmit(e: React.MouseEvent): Promise<void> {
        e.preventDefault();

        let passwordElement = document.getElementById("password") as HTMLInputElement;
        let passwordVerifyElement = document.getElementById("passwordVerify") as HTMLInputElement;
        let firstNameElement = document.getElementById("firstName") as HTMLInputElement;
        let lastNameElement = document.getElementById("lastName") as HTMLInputElement;
        let emailElement = document.getElementById("emailAddress") as HTMLInputElement;
        let dobElement = document.getElementById("dob") as HTMLInputElement;
        let res = await requestHelper.register(
            firstNameElement.value,
            lastNameElement.value,
            passwordElement.value,
            new Date(dobElement.value),
            emailElement.value
        );

        if (res.status != 200) {
            window.alert("Invalid credentials");
        } else {
            store.dispatch(authenticate());
            nav("/");
        }
    }

    return (
        <section className="vh-100 gradient-custom">
            <div className="container py-5 h-100">
                <div className="row justify-content-center align-items-center h-100">
                    <div className="col-12 col-lg-9 col-xl-7">
                        <div
                            className="card shadow-2-strong card-registration card bg-dark text-white"
                            style={{ borderRadius: "15px" }}
                        >
                            <div className="card-body p-4 p-md-5">
                                <h3 className="mb-4 pb-2 pb-md-0 mb-md-5">Registration Form</h3>
                                <form style={{ margin: "0 auto" }}>
                                    <div className="row">
                                        <div className="form-outline">
                                            <input
                                                required
                                                type="text"
                                                id="firstName"
                                                className="form-control form-control-lg"
                                            />
                                            <label className="form-label" htmlFor="firstName">
                                                First Name
                                            </label>
                                        </div>
                                        <div className="form-outline">
                                            <input
                                                required
                                                type="text"
                                                id="lastName"
                                                className="form-control form-control-lg"
                                            />
                                            <label className="form-label" htmlFor="lastName">
                                                Last Name
                                            </label>
                                        </div>
                                        <div className="form-outline">
                                            <input
                                                required
                                                type="date"
                                                id="dob"
                                                className="form-control form-control-lg"
                                            />
                                            <label className="form-label" htmlFor="birthday">
                                                Date of Birth
                                            </label>
                                        </div>
                                        <div className="form-outline">
                                            <input
                                                required
                                                type="password"
                                                id="password"
                                                className="form-control form-control-lg"
                                            />
                                            <label className="form-label" htmlFor="password">
                                                Password
                                            </label>
                                        </div>
                                        <div className="form-outline">
                                            <input
                                                required
                                                type="password"
                                                id="passwordVerify"
                                                className="form-control form-control-lg"
                                            />
                                            <label className="form-label" htmlFor="passwordVerify">
                                                Verify Password
                                            </label>
                                        </div>
                                        <div className="form-outline ">
                                            <input
                                                required
                                                type="email"
                                                id="emailAddress"
                                                className="form-control form-control-lg"
                                            />
                                            <label className="form-label" htmlFor="emailAddress">
                                                Email
                                            </label>
                                        </div>
                                        <div className="mt-4 pt-2">
                                            <input
                                                className="btn btn-outline-light btn-lg px-5"
                                                type="submit"
                                                value="Submit"
                                                onClick={(e) => handleSubmit(e)}
                                            />
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
