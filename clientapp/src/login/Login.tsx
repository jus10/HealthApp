import React, { useEffect, useState } from "react";
import { requestHelper } from "../utils/request";
import "../css/LoginForm.css";
import { store } from "../store";
import { authenticate } from "../store/user";
import { useNavigate } from "react-router-dom";

export function Login() {
    const [loaded, setLoaded] = useState(false);
    const nav = useNavigate();

    useEffect(() => {
        const googleLoginScript = document.createElement("script");
        googleLoginScript.async = false;
        googleLoginScript.type = "text/javascript";
        googleLoginScript.src = "https://apis.google.com/js/platform.js";
        googleLoginScript.addEventListener("load", () => setLoaded(true));
        document.head.appendChild(googleLoginScript);

        const onSignInScript = document.createElement("script");
        onSignInScript.async = false;
        onSignInScript.type = "text/javascript";
        onSignInScript.src = "scripts/google-load.js";
        onSignInScript.addEventListener("load", () => setLoaded(true));
        document.head.appendChild(onSignInScript);
    }, []);

    async function handleSubmit(e: React.MouseEvent): Promise<void> {
        e.preventDefault();
        let emailElement = document.getElementById("email") as HTMLInputElement;
        let passwordElement = document.getElementById("password") as HTMLInputElement;
        if (emailElement != null && passwordElement != null) {
            let email = emailElement.value;
            let pass = passwordElement.value;
            let res = await requestHelper.login(email, pass);

            if (res.status != 200) {
                window.alert("Invalid credentials");
            } else {
                let key = await res.json();
                store.dispatch(authenticate());
                localStorage.setItem("jwt_token", key);
                localStorage.setItem("user", email);
                nav("/");
            }
        }
    }

    return (
        <section className="login vh-100 gradient-custom">
            <meta
                name="google-signin-client_id"
                content="922845578840-ru2q64pmrketedf687hblunqan20av7e.apps.googleusercontent.com"
            ></meta>
            <div className="py-5 h-100">
                <div className="row d-flex justify-content-center align-items-center h-100">
                    <div className="col-12 col-md-8 col-lg-6 col-xl-5">
                        <div className="card bg-dark text-white" style={{ borderRadius: "1em" }}>
                            <div className="card-body p-5 text-center">
                                <div className="mb-md-5 mt-md-4 pb-5">
                                    <h2 className="fw-bold mb-2 text-uppercase">Login</h2>
                                    <p className="text-white-50 mb-5"></p>
                                    <div className="form-outline form-white mb-4">
                                        <input type="email" id="email" className="form-control form-control-lg" />
                                        <label className="form-label" htmlFor="typeEmailX">
                                            Email
                                        </label>
                                        <input type="password" id="password" className="form-control form-control-lg" />
                                        <label className="form-label" htmlFor="typePasswordX">
                                            Password
                                        </label>
                                    </div>

                                    <p className="small mb-5 pb-lg-2">
                                        <a className="text-white-50" href="#!">
                                            Forgot password?
                                        </a>
                                    </p>
                                    <button
                                        className="btn btn-outline-light btn-lg px-5"
                                        type="submit"
                                        onClick={(e) => handleSubmit(e)}
                                    >
                                        Login
                                    </button>

                                    <div className="d-flex justify-content-center text-center mt-4 pt-1">
                                        <div className="g-signin2" data-onsuccess="onSignIn" data-theme="light"></div>
                                    </div>
                                </div>
                                <div>
                                    <p className="mb-0">
                                        Don't have an account?{" "}
                                        <a href="register" className="text-white-50 fw-bold">
                                            Sign Up
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
}
