import { Navbar, Container, Nav } from "react-bootstrap";
import { ApplicationState } from "../store";

import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

import "bootstrap/dist/css/bootstrap.min.css";
import "../css/NavigationBar.css";

export function NavigationBar() {
    let state = useSelector((state: ApplicationState) => state.isLoggedIn);
    let isLoggedIn;
    if (state) {
        isLoggedIn = (
            <Nav className="justify-content-end">
                <Nav.Link as={Link} to="/">
                    {localStorage.getItem("user")}
                </Nav.Link>
                <Nav.Link href="/signout">Sign out</Nav.Link>
            </Nav>
        );
    } else {
        isLoggedIn = (
            <Nav className="justify-content-end">
                <Nav.Link href="/login">Login</Nav.Link>
            </Nav>
        );
    }

    return (
        <Navbar expand="lg">
            <Container>
                <Navbar.Brand href="/">HealthApp</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="me-auto">
                        <Nav.Link as={Link} to="/">
                            Home
                        </Nav.Link>
                        <Nav.Link as={Link} to="/dev">
                            Dev
                        </Nav.Link>
                        <Nav.Link as={Link} to="/scopes">
                            Scopes
                        </Nav.Link>
                        <Nav.Link as={Link} to="/fitness">
                            Fitness
                        </Nav.Link>
                    </Nav>
                    {isLoggedIn}
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}
