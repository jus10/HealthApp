import React, { useState } from "react";
import { Button, FormCheck, Modal } from "react-bootstrap";
import BootstrapTable from "react-bootstrap-table-next";
// @ts-ignore
import cellEditFactory from "react-bootstrap-table2-editor";

import "../css/Routine.css";

interface NewActivity {
    tableRowId: number;
    label: string;
    totalSets: number;
    numberofRepetitions: number;
}

export function Routine() {
    const [show, setShow] = useState(false);
    const [days, setDays] = useState<string[]>([]);
    const [activities, addActivity] = useState<NewActivity[]>([
        {
            tableRowId: 0,
            label: "Enter Activity Name",
            numberofRepetitions: 0,
            totalSets: 0
        }
    ]);

    function addDays(e: React.MouseEvent): void {
        let elementId = e.currentTarget.id;
        let element = document.getElementById(elementId) as HTMLInputElement;
        if (element.checked) {
            setDays([...days, element.id]);
        } else {
            var idx = days.indexOf(element.id);
            setDays(days.splice(idx, 1));
        }
    }

    function addNewActivity(newActivity: NewActivity): void {
        addActivity([...activities, newActivity]);
    }

    function addActivityRow(): void {
        let id = activities.length;
        let newActivity: NewActivity = {
            tableRowId: id,
            label: "Enter Activity Name",
            numberofRepetitions: 0,
            totalSets: 0
        };
        addNewActivity(newActivity);
    }

    function handleClose(): void {
        clear();
    }

    function handleShow(): void {
        setShow(true);
    }

    function handleOk(): void {
        let label = document.getElementById("routineLabel") as HTMLInputElement;
        let requestBody = {
            label: label.value,
            days: days,
            activities: activities,
            User: {
                email: "dev@gmail.com"
            },
            personalRecord: 255
        };

        fetch("/api/v1/fitness/addroutine", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify(requestBody)
        }).catch((e) => {
            console.log(e);
        });

        clear();
    }

    function clear(): void {
        setShow(false);
        activities.splice(0, activities.length);
        days.splice(0, days.length);
    }

    return (
        <>
            <Button style={{ marginLeft: "25px" }} variant="primary" onClick={handleShow}>
                Add Routine
            </Button>
            <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false}>
                <Modal.Header closeButton>
                    <Modal.Title>Add a new routine</Modal.Title>
                </Modal.Header>
                <Modal.Body className="modalBody">
                    <form>
                        <label htmlFor="routineLabel">Routine Name:</label>
                        <input
                            type="text"
                            id="routineLabel"
                            name="routineLabel"
                            style={{ width: "100px", marginLeft: "20px" }}
                        ></input>
                    </form>
                    <div>
                        <BootstrapTable
                            data={activities}
                            columns={[
                                {
                                    dataField: "label",
                                    text: "Activity"
                                },
                                {
                                    dataField: "totalSets",
                                    text: "Sets"
                                },
                                {
                                    dataField: "numberofRepetitions",
                                    text: "Repetitions"
                                }
                            ]}
                            keyField="tableRowId"
                            cellEdit={cellEditFactory({ mode: "click", blurToSave: true })}
                            noDataIndication={() => "Click add to create add a new activity"}
                        ></BootstrapTable>
                        <button style={{ float: "right" }} onClick={addActivityRow}>
                            Add
                        </button>
                    </div>
                    <div style={{ marginTop: "50px" }}>
                        <FormCheck
                            id="mon"
                            inline
                            onClick={(ev: React.MouseEvent) => addDays(ev)}
                            label="Mon"
                        ></FormCheck>
                        <FormCheck
                            id="tue"
                            inline
                            onClick={(ev: React.MouseEvent) => addDays(ev)}
                            label="Tue"
                        ></FormCheck>
                        <FormCheck
                            id="wed"
                            inline
                            onClick={(ev: React.MouseEvent) => addDays(ev)}
                            label="Wed"
                        ></FormCheck>
                        <FormCheck
                            id="thu"
                            inline
                            onClick={(ev: React.MouseEvent) => addDays(ev)}
                            label="Thu"
                        ></FormCheck>
                        <FormCheck
                            id="fri"
                            inline
                            onClick={(ev: React.MouseEvent) => addDays(ev)}
                            label="Fri"
                        ></FormCheck>
                        <FormCheck
                            id="sat"
                            inline
                            onClick={(ev: React.MouseEvent) => addDays(ev)}
                            label="Sat"
                        ></FormCheck>
                        <FormCheck
                            id="sun"
                            inline
                            onClick={(ev: React.MouseEvent) => addDays(ev)}
                            label="Sun"
                        ></FormCheck>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={handleOk}>
                        Ok
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}
