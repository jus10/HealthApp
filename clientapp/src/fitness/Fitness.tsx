import { ActivityTable, RoutineInfo } from "./ActivityTable";
import { RoutineData, RoutineTable } from "./RoutineTable";

import { useEffect, useState } from "react";
import Calendar from "react-calendar";

import "react-calendar/dist/Calendar.css";
import "../css/FitnessForm.css";

export function Fitness() {
    const defaultRoutineInfo: RoutineInfo = {
        days: [],
        id: -1,
        label: "",
        personalRecord: -1,
        targetRepetitions: -1,
        totalSets: -1,
        activities: [],
        datePerformed: new Date()
    };

    const [hoverIdx] = useState(-1);
    const [selectedDate, setSelectedDate] = useState(new Date());
    const [currentDate, setCurrentDate] = useState(new Date());
    const [routines, setRoutines] = useState<Array<RoutineData>>([]);
    const [currentRoutine, setCurrentRoutine] = useState<RoutineInfo>(defaultRoutineInfo);

    function setDate(date: Date): void {
        setSelectedDate(date);
    }

    function updateCurrentRoutine(routine: RoutineInfo): void {
        setCurrentRoutine(routine);
    }

    function onChange(date: Date): void {
        setDate(date);
        fetchRoutine(date);
    }

    function updateRoutines(routines: Array<RoutineData>): void {
        setRoutines(routines);
    }

    async function fetchRoutine(date: Date): Promise<void> {
        const res = await fetch("/api/v1/fitness/routinebydate?date=" + date.toLocaleDateString());
        if (res.ok) {
            let data = await res.json();
            updateCurrentRoutine(data);
        } else {
            updateCurrentRoutine(defaultRoutineInfo);
        }
    }

    async function fetchRoutines(): Promise<void> {
        const res = await fetch("/api/v1/fitness/routines");
        const data: Array<RoutineData> = await res.json();
        updateRoutines(data);
    }

    useEffect(() => {
        fetchRoutine(currentDate);
    }, []);

    return (
        <div>
            <div className="container">
                <Calendar className={"calendar"} onChange={(e: Date) => onChange(e)} value={selectedDate}></Calendar>
            </div>
            <div className="fitnessTables">
                <RoutineTable routines={routines}></RoutineTable>
                <ActivityTable routine={currentRoutine} date={selectedDate}></ActivityTable>
            </div>
        </div>
    );
}
