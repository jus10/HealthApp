import { Sets } from "./Sets";

import { useEffect, useState } from "react";
import BootstrapTable from "react-bootstrap-table-next";
// @ts-ignore
import cellEditFactory from "react-bootstrap-table2-editor";

import "../css/ActivityTable.css";
import { Button } from "react-bootstrap";
import { transformTableData } from "../utils/tabletransform";

export interface RoutineInfo {
    id: number;
    label: string;
    days: Array<string>;
    datePerformed: Date;
    personalRecord: number;
    totalSets: number;
    targetRepetitions: number;
    activities: Array<Activity>;
}

export interface Activity {
    id?: number;
    label: string;
    sets: Array<Sets>;
    datePerformed: Date;
    personalRecord: number;
}

interface Props {
    date: Date;
    routine: RoutineInfo;
}

export function ActivityTable(props: Props) {
    const days = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];
    const [hoverIdx, setIdx] = useState(-1);
    const [currentDate, setDate] = useState(new Date());
    const [activities, setActivities] = useState<Array<Activity>>([]);
    const [tableMap, setTableMap] = useState<Map<string, any>>(new Map());
    const [routine, setRoutine] = useState<RoutineInfo>();

    function updateCurrentDate(date: Date): void {
        setDate(date);
    }

    const expandRow = {
        renderer: (row: any) => (
            <BootstrapTable
                classes="nestedActivityTable"
                data={tableMap.get(row.label)}
                columns={[
                    { dataField: "setId", text: "Set" },
                    { dataField: "repetitions", text: "Repetitions" },
                    { dataField: "weight", text: "Weight (lbs)" }
                ]}
                keyField={"setId"}
                cellEdit={cellEdit}
                noDataIndication={() => "Click add to create add a new activity"}
            ></BootstrapTable>
        )
    };

    const cellEdit = cellEditFactory({
        afterSaveCell: (oldValue: any, newValue: any, row: any, column: any) => {
            let activityToUpdate = routine?.activities.find((a) => a.label === row.parent);
            let setToUpdate = activityToUpdate?.sets.at(row.setId - 1);

            switch (column.dataField.toLowerCase()) {
                // Set #
                case 0:
                    break;
                // Repetitions
                case "repetitions":
                    setToUpdate!.repetitions = newValue;
                    break;
                // Weight
                case "weight":
                    setToUpdate!.weight = newValue;
                    break;
            }
        },
        blurToSave: true,
        mode: "click"
    });

    const rowEvents = {
        onMouseEnter: (e: any, row: any, rowIndex: any) => {
            setHoverIdx(rowIndex);
        },
        onMouseLeave: () => {
            setHoverIdx(-1);
        }
    };

    function updateRoutine(routine: RoutineInfo) {
        setRoutine(routine);
    }

    function saveActivities() {
        routine!.datePerformed = currentDate;
        fetch("/api/v1/fitness/updateactivities", {
            method: "PUT",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify(routine)
        });
    }

    function setHoverIdx(idx: number): void {
        setIdx(idx);
    }

    function updateTableActivities(data: any): void {
        setActivities(data);
    }

    function addSetRows(label: string, setsData: Array<Sets> | null) {
        if (setsData != null) {
            for (let set of setsData) {
                if (set != null) {
                    if (!tableMap.has(label)) {
                        tableMap.set(label, []);
                    }
                    tableMap.get(label)!.push({
                        parent: label,
                        setId: set.setId,
                        repetitions: set.repetitions,
                        weight: set.weight
                    });
                }
            }
        }
    }

    useEffect(() => {
        tableMap.clear();
        activities.splice(0, activities.length);

        if (props.routine !== undefined) {
            let mapped = props.routine.activities!.reduce((map: any, obj: any) => {
                map[obj.label] = obj;
                return map;
            }, {});

            // Transform data to fit react table data format [{}]
            let newData = transformTableData(mapped);
            for (let i of props.routine.activities!) {
                addSetRows(i.label, i.sets);
            }

            updateCurrentDate(props.date);
            updateTableActivities(newData!.flat());
            updateRoutine(props.routine);
        }
    }, [props]);

    return (
        <div className="activityTable center" style={{ display: "block" }}>
            <BootstrapTable
                keyField="tableRowId"
                data={activities}
                columns={[{ dataField: "label", text: "Activity" }]}
                classes="activityTable"
                rowEvents={rowEvents}
                expandRow={expandRow}
                noDataIndication={() => "No recorded data for this date"}
            ></BootstrapTable>
            <Button variant="primary" onClick={saveActivities}>
                Save
            </Button>
        </div>
    );
}
