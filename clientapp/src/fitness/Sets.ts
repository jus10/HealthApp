export type Sets = {
    setId: number;
    repetitions: number;
    weight: number;
};
