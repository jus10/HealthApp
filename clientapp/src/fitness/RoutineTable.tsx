import { useEffect, useState } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import { transformTableData } from "../utils/tabletransform";
import { Activity } from "./ActivityTable";
import { Routine } from "./Routine";

export interface RoutineData {
    label: string;
    activities: Array<Activity>;
    dateCreated: Date;
    dateUpdated: Date;
}

interface TableData {
    id: number;
    label: string;
    days: Array<string>;
    personalRecord: number;
    totalSets: number;
    targetRepetitions: number;
}

interface Props {
    routines: Array<RoutineData>;
}

export const RoutineTable = (props: Props) => {
    const [routines, setRoutines] = useState<Array<TableData>>([]);
    const [tableMap, setTableMap] = useState<Map<string, any>>(new Map());

    function updateRoutines(newRoutines: any) {
        setRoutines(newRoutines);
    }

    function updateTableMap(arg: any) {
        setTableMap(arg);
    }

    useEffect(() => {
        tableMap.clear();
        routines.splice(0, routines.length);

        let mapped = props.routines.reduce((map: any, obj: any) => {
            map[obj.label] = obj;
            return map;
        }, {});

        let newData = transformTableData(mapped);
        updateRoutines(newData!.flat());
        // updateTableMap(mapped);
    }, [props.routines]);

    return (
        <div className="center" style={{ display: "block" }}>
            <BootstrapTable
                keyField="tableRowId"
                data={routines}
                columns={[
                    { dataField: "label", text: "Routines" },
                    { dataField: "days", text: "Days" },
                    { dataField: "totalSets", text: "Total Sets" },
                    { dataField: "targetRepetitions", text: "Target Repetitions" },
                    { dataField: "personalRecord", text: "Personal Record" }
                ]}
                classes="activityTable"
                noDataIndication={() => "No routines available"}
            ></BootstrapTable>
            <Routine></Routine>
        </div>
    );
};
