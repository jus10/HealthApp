import { App } from "./App";
import { store } from "./store";

import React, { StrictMode } from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, unstable_HistoryRouter, useNavigate } from "react-router-dom";
import reportWebVitals from "./reportWebVitals";

import "./css/index.css";

ReactDOM.render(
    <BrowserRouter>
        <App store={store} />
    </BrowserRouter>,
    document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
