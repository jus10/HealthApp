using Grpc.Net.Client;
using HealthApiGateway.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProtoBuf.Grpc.Client;
using Shared.Contracts;
using Shared.Models;

namespace HealthApiGateway.Controllers
{
    /// <summary>
    /// This controller contains all the public endpoints used to interact with our gRPC service
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("/api/v1/[controller]")]
    public sealed class HealthController : ControllerBase
    {
        private static readonly GrpcChannel CHANNEL = GrpcChannel.ForAddress("https://localhost:7079");

        private readonly ITokenService _tokenService;

        private readonly IConfiguration _config;

        private readonly ILogger<HealthController> _logger;

        public HealthController(ITokenService tokenService, IConfiguration config, ILogger<HealthController> logger)
        {
            _logger = logger;
            _tokenService = tokenService;
            _config = config;
        }

        [HttpGet]
        [Route("")]
        public IActionResult Get()
        {
            return new JsonResult("200");
        }

        [HttpGet]
        [Route("body/weight")]
        public IActionResult GetBodyWeight()
        {
            var jwtToken = HttpContext.Session.GetString("JwtToken");
            if (!_tokenService.IsTokenValid(jwtToken))
            {
                return Unauthorized();
            }

            if (HttpContext.Session.IsAvailable)
            {
                var serializedToken = System.Text.Encoding.UTF8.GetString(HttpContext.Session.Get("token") ?? throw new InvalidDataException("Token not found."));
                var token = JsonConvert.DeserializeObject<AccessToken>(serializedToken) ?? throw new InvalidCastException("Invalid token");

                var client = CHANNEL.CreateGrpcService<IHealthService>();
                var response = client.GetBodyWeight(new GoogleFitRequest
                {
                    StartDate = DateTime.UtcNow.AddDays(-7),
                    EndDate = DateTime.UtcNow,
                    Token = token
                }).Result;

                return Ok(JsonConvert.SerializeObject(response));
            }
            else
            {
                return NotFound("Token has expired, reauthenticate");
            }
        }
    }
}