using Grpc.Net.Client;
using HealthApiGateway.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProtoBuf.Grpc.Client;
using Shared.Contracts;
using Shared.Models;

namespace HealthApiGateway.Controllers
{
    /// <summary>
    /// This controller contains all the endpoints required to authenticate with our service
    /// </summary>
    [ApiController]
    [Authorize]
    [Route("[controller]")]
    public sealed class AuthController : ControllerBase
    {
        private static readonly GrpcChannel CHANNEL = GrpcChannel.ForAddress("https://localhost:7079");

        private readonly ITokenService _tokenService;

        private readonly IConfiguration _config;

        private readonly ILogger<AuthController> _logger;

        public AuthController(IConfiguration config, ITokenService token, ILogger<AuthController> logger)
        {
            _config = config;
            _tokenService = token;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("login")]
        public IActionResult Login([FromBody] UserInfo user)
        {
            _logger.LogInformation($"User login attempt from: {user.Email}");
            var client = CHANNEL.CreateGrpcService<IAuthService>();

            var response = client.BasicAuthentication(user).Result;
            if (!response.IsAuthenticated)
            {
                return BadRequest();
            }
            // If successful, send a JWT token back to user and set session
            var jwtToken = _tokenService.BuildToken(user.Email);
            HttpContext.Session.SetString("JwtToken", jwtToken);
            HttpContext.Session.SetString("UserEmail", user.Email);
            HttpContext.Session.SetInt32("UserId", response.UserId);

            _logger.LogInformation($"User logged in: {user.Email}");
            return Ok(jwtToken);
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("register")]
        public IActionResult Register([FromBody] UserInfo user)
        {
            _logger.LogInformation($"Registering user {user.FirstName}, {user.LastName}, {user.Email}, {user.DateOfBirth}");
            var client = CHANNEL.CreateGrpcService<IAuthService>();
            var response = client.RegisterUser(user).Result;
            // User will be automatically be authenticated
            HttpContext.Session.SetInt32("UserId", response.UserId);

            return Ok("/home");
        }

        [HttpGet]
        [Route("oauth2callback")]
        public IActionResult OAuth2Callback([FromQuery] string code, [FromQuery] string scope)
        {
            var jwtToken = HttpContext.Session.GetString("JwtToken");
            if (!_tokenService.IsTokenValid(jwtToken))
            {
                return Unauthorized();
            }

            var token = GoogleAuthHelper.ExchangeAuthCode(_config["Health:ServiceClientId"], _config["Health:ServiceClientSecret"], code).Result;
            if (token == null)
            {
                return BadRequest(400);
            }

            HttpContext.Session.SetString("token", JsonConvert.SerializeObject(token));
            var client = CHANNEL.CreateGrpcService<IAuthService>();
            client.UpdateUserToken(
                new UserAccessToken
                {
                    UserId = HttpContext.Session.GetInt32("UserId").Value,
                    Scope = scope,
                    Token = new AccessToken
                    {
                        Token = token.Token,
                        RefreshToken = token.RefreshToken,
                        ExpiresIn = token.ExpiresIn,
                        Type = token.Type
                    }
                });
            return Redirect("https://localhost:3000");
        }

        [EnableCors("Cors")]
        [HttpGet]
        [Route("authorize")]
        public IActionResult Authorize([FromQuery] string scope)
        {
            var jwtToken = HttpContext.Session.GetString("JwtToken");
            if (!_tokenService.IsTokenValid(jwtToken))
            {
                return Unauthorized();
            }

            var redirectUri = GoogleAuthHelper.BuildAuthUri(_config["Health:ServiceClientId"], scope);
            if (redirectUri == null)
            {
                return BadRequest(400);
            }

            return Redirect(redirectUri.AbsoluteUri);
        }
    }
}