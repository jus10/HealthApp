using System.Globalization;
using System.Linq.Expressions;
using Grpc.Net.Client;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProtoBuf.Grpc.Client;
using Shared.Contracts;
using Shared.Models;
using Shared.Models.Fitness;

namespace HealthApiGateway.Controllers
{
    /// <summary>
    /// This controller contains all the endpoints required to authenticate with our service
    /// </summary>
    [ApiController]
    // [Authorize]
    [AllowAnonymous]
    [Route("/api/v1/[controller]")]
    public sealed class FitnessController : ControllerBase
    {
        private static readonly GrpcChannel CHANNEL = GrpcChannel.ForAddress("https://localhost:7079");

        private readonly ILogger<FitnessController> _logger;

        public FitnessController(ILogger<FitnessController> logger)
        {
            _logger = logger;
        }

        [Route("")]
        [HttpGet]
        public IActionResult Get()
        {
            return Ok("/home");
        }

        [Route("addroutine")]
        [HttpPost]
        public IActionResult AddRoutine([FromBody] Routine routine)
        {
            var client = CHANNEL.CreateGrpcService<IFitnessService>();
            routine.User = new User
            {
                Id = HttpContext.Session.GetInt32("UserId").Value
            };

            client.AddRoutine(routine);

            return Ok();
        }

        [Route("routinebydate")]
        [HttpGet]
        public async Task<IActionResult> GetRoutineByDate([FromQuery] DateTime date)
        {
            if (!HttpContext.Session.IsAvailable)
            {
                return NotFound();
            }

            var client = CHANNEL.CreateGrpcService<IFitnessService>();
            var userId = HttpContext.Session.GetInt32("UserId").Value;
            try
            {
                var res = await client.GetRoutineByDate(new() { User = new() { Id = userId }, DatePerformed = date });
                return Ok(res);
            }
            catch (Exception e)
            {
                _logger.LogInformation(e.Message);
                return NotFound();
            }
        }

        [Route("routines")]
        [HttpGet]
        public IActionResult GetRoutines()
        {
            if (!HttpContext.Session.IsAvailable)
            {
                return NotFound();
            }

            var client = CHANNEL.CreateGrpcService<IFitnessService>();
            var userId = HttpContext.Session.GetInt32("UserId").Value;
            var res = client.GetRoutines(new() { Id = userId }).Result;
            return Ok(res);
        }

        [Route("updateactivities")]
        [HttpPut]
        public IActionResult UpdateActivities([FromBody] Routine routine)
        {
            if (!HttpContext.Session.IsAvailable)
            {
                return NotFound();
            }

            var client = CHANNEL.CreateGrpcService<IFitnessService>();
            var userId = HttpContext.Session.GetInt32("UserId").Value;

            routine.User = new() { Id = userId };

            client.UpdateActivities(routine);
            return Ok(200);
        }
    }
}