﻿namespace HealthApiGateway.Core
{
    public interface ITokenService
    {
        string BuildToken(string email);

        bool IsTokenValid(string token);
    }
}