using Shared.Models;
using System.Net.Http.Headers;

namespace HealthApiGateway.Core
{
    /// <summary>
    /// Helper class used to generate Google OAuth2 requests
    /// </summary>
    internal sealed class GoogleAuthHelper
    {
        private static readonly string HOSTNAME = "localhost";

        private static readonly string PORT = "5001";

        private static readonly string BASE_API_PATH = "https://www.googleapis.com/auth/";

        private static readonly HttpClient _client = new HttpClient();

        public static Uri BuildAuthUri(string clientId, string scope)
        {
            var authUri = "https://accounts.google.com/o/oauth2/auth";
            var requestParams = new Dictionary<string, string>()
            {
                { "redirect_uri", $"https://{HOSTNAME}:{PORT}/auth/oauth2callback" },
                { "response_type", "code" },
                { "scope", BASE_API_PATH + scope },
                { "prompt", "consent" },
                { "access_type", "offline" },
                { "client_id", clientId },
            };

            var qs = new QueryString();
            foreach (var kvp in requestParams) qs = qs.Add(kvp.Key, kvp.Value);

            return new(authUri + qs.ToString());
        }

        public static async Task<AccessToken> ExchangeAuthCode(string clientId, string clientSecret, string code)
        {
            var tokenUri = "https://accounts.google.com/o/oauth2/token";
            var requestParams = new Dictionary<string, string>()
            {
                { "redirect_uri", $"https://{HOSTNAME}:{PORT}/auth/oauth2callback" },
                { "code", code },
                { "grant_type", "authorization_code" },
                { "client_id", clientId },
                { "client_secret", clientSecret }
            };

            var content = new FormUrlEncodedContent(requestParams);
            content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
            var response = await _client.PostAsync(tokenUri, content);

            return await response.Content.ReadFromJsonAsync<AccessToken>() ?? throw new Exception("Could not retrieve an access token");
        }
    }
}