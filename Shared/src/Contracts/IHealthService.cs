using Shared.Models;
using System.ServiceModel;

namespace Shared.Contracts
{
    [ServiceContract]
    public interface IHealthService
    {
        [OperationContract]
        Task<GoogleFitResponse> GetBodyWeight(GoogleFitRequest request);

        [OperationContract]
        Task<GoogleFitResponse> GetHeartRate(GoogleFitRequest request);
    }
}