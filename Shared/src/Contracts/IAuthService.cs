using System.ServiceModel;
using Shared.Models;

namespace Shared.Contracts
{
    [ServiceContract]
    public interface IAuthService
    {
        [OperationContract]
        Task<AuthResponse> BasicAuthentication(UserInfo userInfo);

        [OperationContract]
        Task<AuthResponse> RegisterUser(UserInfo userInfo);

        [OperationContract]
        Task<Empty> UpdateUserToken(UserAccessToken uat);
    }
}