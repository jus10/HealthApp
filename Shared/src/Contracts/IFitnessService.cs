using System.ServiceModel;
using Shared.Models;
using Shared.Models.Fitness;

namespace Shared.Contracts
{
    [ServiceContract]
    public interface IFitnessService
    {
        [OperationContract]
        Task<Empty> AddRoutine(Routine request);

        [OperationContract]
        Task<ICollection<Routine>> GetRoutines(User user);

        [OperationContract]
        Task<Routine?> GetRoutineByDate(Routine request);

        [OperationContract]
        Task<Empty> UpdateActivities(Routine request);
    }
}