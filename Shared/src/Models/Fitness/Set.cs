using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Shared.Models.Fitness
{
    [DataContract]
    [Table("set")]
    public class Set : Entity
    {
        [DataMember(Order = 1)]
        public int SetId { get; set; }

        [DataMember(Order = 2)]
        public int Repetitions { get; set; }

        [DataMember(Order = 3)]
        public double Weight { get; set; }

        [ForeignKey("ActivityId")]
        public virtual Activity Activity { get; set; }

        [ForeignKey("RoutineId")]
        public virtual Routine Routine { get; set; }
    }
}