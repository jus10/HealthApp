using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

namespace Shared.Models.Fitness
{
    [DataContract]
    [Table("routine")]
    public class Routine : Entity
    {
        [DataMember(Order = 1)]
        public string Label { get; set; }

        [DataMember(Order = 2)]
        public List<string> Days { get; set; }

        [DataMember(Order = 3)]
        [Column(TypeName = "Date")]
        public DateTime DatePerformed { get; set; }

        [DataMember(Order = 4)]
        public ICollection<Activity> Activities { get; set; } = default;

        [ForeignKey("UserId")]
        [DataMember(Order = 5)]
        public User? User { get; set; }
    }
}