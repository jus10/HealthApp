using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

#pragma warning disable CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.
namespace Shared.Models.Fitness
{
    [DataContract]
    [Table("activity")]
    public class Activity : Entity
    {
        [DataMember(Order = 1)]
        public string Label { get; set; }

        [DataMember(Order = 2)]
        public List<Set>? Sets { get; set; }

        [DataMember(Order = 3)]
        public double PersonalRecord { get; set; }

        [DataMember(Order = 4)]
        public int TotalSets { get; set; }

        [DataMember(Order = 5)]
        public int NumberOfRepetitions { get; set; }

        public ICollection<Routine> Routines { get; set; }
    }
}
#pragma warning restore CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.