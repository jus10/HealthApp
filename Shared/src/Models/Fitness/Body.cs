namespace Shared.Models.Fitness
{
    public class Body
    {
        public int Id { get; set; }
        public double Weight { get; set; }
        public double Height { get; set; }
        public double HeartRate { get; set; }
    }
}