using System.Runtime.Serialization;

namespace Shared.Models
{
    [DataContract]
    public class DateRequest
    {
        [DataMember(Order = 1)]
        public DateTime Date { get; set; }
    }
}