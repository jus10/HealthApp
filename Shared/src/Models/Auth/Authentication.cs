﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

#pragma warning disable CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.

namespace Shared.Models
{
    [DataContract]
    [Table("authentication")]
    public class Authentication : Entity
    {
        [DataMember(Order = 1)]
        public string Password { get; set; }

        [DataMember(Order = 2)]
        public string Salt { get; set; }

        [DataMember(Order = 3)]
        public List<string>? Scopes { get; set; }

        [DataMember(Order = 4)]
        public string? AccessToken { get; set; }

        [DataMember(Order = 5)]
        public string? RefreshToken { get; set; }

        [DataMember(Order = 6)]
        public double? ExpiresIn { get; set; }

        [DataMember(Order = 7)]
        public string? TokenType { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }
    }
}
#pragma warning restore CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.