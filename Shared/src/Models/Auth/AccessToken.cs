using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace Shared.Models
{
    [DataContract]
    public class AccessToken
    {
        [JsonPropertyName("access_token")]
        [DataMember(Order = 1)]
        public string Token { get; set; }

        [JsonPropertyName("expires_in")]
        [DataMember(Order = 2)]
        public double ExpiresIn { get; set; }

        [JsonPropertyName("token_type")]
        [DataMember(Order = 3)]
        public string Type { get; set; }

        [JsonPropertyName("refresh_token")]
        [DataMember(Order = 4)]
        public string RefreshToken { get; set; }
    }
}