using System.Runtime.Serialization;

#pragma warning disable CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.
namespace Shared.Models
{
    [DataContract]
    public class UserInfo
    {
        [DataMember(Order = 1)]
        public int Id { get; set; }

        [DataMember(Order = 2)]
        public string? DateOfBirth { get; set; }

        [DataMember(Order = 3)]
        public string? FirstName { get; set; }

        [DataMember(Order = 4)]
        public string? LastName { get; set; }

        [DataMember(Order = 5)]
        public string? Email { get; set; }

        [DataMember(Order = 6)]
        public string? GoogleId { get; set; }

        [DataMember(Order = 7)]
        public string? Password { get; set; }
    }
}
#pragma warning restore CS8632 // The annotation for nullable reference types should only be used in code within a '#nullable' annotations context.