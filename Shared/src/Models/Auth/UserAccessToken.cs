using System.Runtime.Serialization;

namespace Shared.Models
{
    [DataContract]
    public class UserAccessToken
    {
        [DataMember(Order = 1)]
        public int UserId { get; set; }

        [DataMember(Order = 2)]
        public AccessToken Token { get; set; }

        [DataMember(Order = 3)]
        public string Scope { get; set; }
    }
}