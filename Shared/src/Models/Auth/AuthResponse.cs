using System.Runtime.Serialization;

namespace Shared.Models
{
    [DataContract]
    public class AuthResponse
    {
        [DataMember(Order = 1)]
        public bool IsAuthenticated { get; set; }

        [DataMember(Order = 2)]
        public int UserId { get; set; }
    }
}