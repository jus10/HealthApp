using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Runtime.Serialization;
using ProtoBuf;
using Shared.Models.Fitness;

namespace Shared.Models
{
    [DataContract]
    [ProtoInclude(400, typeof(User))]
    [ProtoInclude(500, typeof(DateRequest))]
    [ProtoInclude(600, typeof(Activity))]
    [ProtoInclude(700, typeof(AuthResponse))]
    [ProtoInclude(800, typeof(Routine))]
    [ProtoInclude(900, typeof(Set))]
    public abstract class Entity
    {
        [Key]
        [DataMember(Order = 1)]
        public int Id { get; set; }

        [DataMember(Order = 2)]
        [Column(TypeName = "Date")]
        public DateTime DateCreated { get; set; }

        [DataMember(Order = 3)]
        [Column(TypeName = "Date")]
        public DateTime? DateUpdated { get; set; }
    }
}