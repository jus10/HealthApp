using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;

using Shared.Models.Fitness;

namespace Shared.Models
{
    [DataContract]
    [Table("user")]
    public class User : Entity
    {
        [DataMember(Order = 1)]
        public string Email { get; set; }

        [DataMember(Order = 2)]
        public string FirstName { get; set; }

        [DataMember(Order = 3)]
        public string LastName { get; set; }

        [DataMember(Order = 4)]
        public int Age;

        public virtual ICollection<Routine> Routines { get; set; }
        public virtual Authentication Authentication { get; set; }
    }
}