using System.Runtime.Serialization;

namespace Shared.Models
{
    [DataContract]
    public class Entry
    {
        [DataMember(Order = 1)]
        public DateTime Date { get; set; }

        [DataMember(Order = 2)]
        public double Value { get; set; }
    }
}