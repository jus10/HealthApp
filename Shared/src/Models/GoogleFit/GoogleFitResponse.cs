using System.Runtime.Serialization;

namespace Shared.Models
{
    [DataContract]
    public class GoogleFitResponse
    {
        [DataMember(Order = 1)]
        public DateTime StartDate { get; set; }

        [DataMember(Order = 2)]
        public DateTime EndDate { get; set; }

        [DataMember(Order = 3)]
        public List<Entry> Values { get; set; }
    }
}