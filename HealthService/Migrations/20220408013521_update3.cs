﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HealthService.Migrations
{
    public partial class update3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActivityRoutine_activity_RoutineId",
                table: "ActivityRoutine");

            migrationBuilder.RenameColumn(
                name: "RoutineId",
                table: "ActivityRoutine",
                newName: "ActivitiesId");

            migrationBuilder.AddForeignKey(
                name: "FK_ActivityRoutine_activity_ActivitiesId",
                table: "ActivityRoutine",
                column: "ActivitiesId",
                principalTable: "activity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ActivityRoutine_activity_ActivitiesId",
                table: "ActivityRoutine");

            migrationBuilder.RenameColumn(
                name: "ActivitiesId",
                table: "ActivityRoutine",
                newName: "RoutineId");

            migrationBuilder.AddForeignKey(
                name: "FK_ActivityRoutine_activity_RoutineId",
                table: "ActivityRoutine",
                column: "RoutineId",
                principalTable: "activity",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
