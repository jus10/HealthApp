﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HealthService.Migrations
{
    public partial class update1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RoutineId",
                table: "set",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_set_RoutineId",
                table: "set",
                column: "RoutineId");

            migrationBuilder.AddForeignKey(
                name: "FK_set_routine_RoutineId",
                table: "set",
                column: "RoutineId",
                principalTable: "routine",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_set_routine_RoutineId",
                table: "set");

            migrationBuilder.DropIndex(
                name: "IX_set_RoutineId",
                table: "set");

            migrationBuilder.DropColumn(
                name: "RoutineId",
                table: "set");
        }
    }
}
