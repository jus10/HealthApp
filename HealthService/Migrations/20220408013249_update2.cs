﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace HealthService.Migrations
{
    public partial class update2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_activity_routine_RoutineId",
                table: "activity");

            migrationBuilder.DropIndex(
                name: "IX_activity_RoutineId",
                table: "activity");

            migrationBuilder.DropColumn(
                name: "RoutineId",
                table: "activity");

            migrationBuilder.CreateTable(
                name: "ActivityRoutine",
                columns: table => new
                {
                    RoutineId = table.Column<int>(type: "integer", nullable: false),
                    RoutinesId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityRoutine", x => new { x.RoutineId, x.RoutinesId });
                    table.ForeignKey(
                        name: "FK_ActivityRoutine_activity_RoutineId",
                        column: x => x.RoutineId,
                        principalTable: "activity",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivityRoutine_routine_RoutinesId",
                        column: x => x.RoutinesId,
                        principalTable: "routine",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActivityRoutine_RoutinesId",
                table: "ActivityRoutine",
                column: "RoutinesId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActivityRoutine");

            migrationBuilder.AddColumn<int>(
                name: "RoutineId",
                table: "activity",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_activity_RoutineId",
                table: "activity",
                column: "RoutineId");

            migrationBuilder.AddForeignKey(
                name: "FK_activity_routine_RoutineId",
                table: "activity",
                column: "RoutineId",
                principalTable: "routine",
                principalColumn: "Id");
        }
    }
}
