using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System.Security.Cryptography;

namespace HealthService.Utils
{
    internal static class CryptoConverter
    {
        internal struct Hashed
        {
            public string Password { get; set; }
            public byte[] Salt { get; set; }
        }

        public static Hashed Encrypt(string plainText)
        {
            using var aes = Aes.Create();
            var salt = RandomNumberGenerator.GetBytes(128 / 8);
            var hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: plainText,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100000,
                numBytesRequested: 256 / 8));

            return new Hashed { Password = hashed, Salt = salt };
        }

        public static string Encrypt(string plainText, string salt)
        {
            using var aes = Aes.Create();
            var hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: plainText,
                salt: StringToByteArray(salt),
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: 100000,
                numBytesRequested: 256 / 8));

            return hashed;
        }

        public static string Decrypt(string val)
        {
            return "";
        }

        private static byte[] StringToByteArray(string hex)
        {
            var bytes = new List<byte>();
            foreach (var val in hex.Split('-'))
            {
                bytes.Add(Convert.ToByte(val, 16));
            }
            return bytes.ToArray();
        }
    }
}