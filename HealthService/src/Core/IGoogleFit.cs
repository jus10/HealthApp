namespace HealthService.Core
{
    public interface IGoogleFit
    {
        Task<string> GetActivity(long startTimeMillis, long endTimeMillis, string source);
    }
}