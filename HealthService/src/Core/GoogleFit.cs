using System.Net.Http.Headers;
using HealthService.Models;

namespace HealthService.Core
{
    public class GoogleFit : IGoogleFit
    {
        private readonly string _uri = "https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate";

        private readonly HttpClient _client = new HttpClient();

        public GoogleFit(ILogger<GoogleFit> logger)
        {

            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "");
        }

        public async Task<string> GetActivity(long startTimeMillis, long endTimeMillis, string source)
        {
            var requestBody = new RequestBody
            {
                AggregateBy = new List<AggregateBy>()
                {
                    new AggregateBy {DataSourceId = "", DataTypeName = ""}
                },
                BucketByTime = new BucketByTime { DurationMillis = 86400000 },
                StartTimeMillis = 1438705622000,
                EndTimeMillis = 1439310422000
            };

            var response = await _client.PostAsJsonAsync<RequestBody>(_uri, requestBody);

            return response.Content.ReadAsStringAsync().Result;
        }
    }
}