using HealthService.Contexts;
using Shared.Contracts;
using Shared.Models;

namespace HealthService.Services
{
    /// <summary>
    /// This file contains business logic related to authentication and registration
    /// </summary>
    public class AuthService : IAuthService
    {
        private readonly IAuthContext _context;

        private readonly ILogger<AuthService> _logger;

        public AuthService(IAuthContext context, ILogger<AuthService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public Task<AuthResponse> RegisterUser(UserInfo request)
        {
            _context.RegisterUser(new User
            {
                Age = DateTime.Today.Year - DateTime.Parse(request.DateOfBirth).Year,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
            },
            new Authentication
            {
                Password = request.Password,
            }
            );
            _context.SaveChanges();
            return Task.FromResult(new AuthResponse { IsAuthenticated = true });
        }

        public Task<AuthResponse> BasicAuthentication(UserInfo userInfo)
        {
            var authResponse = _context.VerifyUser(userInfo);
            return Task.FromResult(authResponse);
        }

        public async Task<Empty> UpdateUserToken(UserAccessToken uat)
        {
            _context.UpdateUserToken(uat);
            await _context.SaveChangesAsync(CancellationToken.None);
            return new Empty();
        }
    }
}