using HealthService.Contexts;
using Shared.Contracts;

namespace HealthService.Services
{
    /// <summary>
    /// This class handles all interactions with the Google Fit API
    /// </summary>
    public partial class GoogleFitService : IHealthService
    {
        private static readonly HttpClient _client = new HttpClient();

        private readonly IHealthContext _context;

        private readonly ILogger<GoogleFitService> _logger;

        public GoogleFitService(IHealthContext context, ILogger<GoogleFitService> logger)
        {
            _context = context;
            _logger = logger;
        }
    }
}