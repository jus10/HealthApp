using Shared.Contracts;
using Shared.Models;

namespace HealthService.Services
{
    /// <summary>
    /// This file handles any interactions with any calls to Google Fits API body scope
    /// </summary>
    public partial class GoogleFitService : IHealthService
    {
        Task<GoogleFitResponse> IHealthService.GetBodyWeight(GoogleFitRequest request)
        {
            return Task.FromResult(new GoogleFitResponse());
        }

        Task<GoogleFitResponse> IHealthService.GetHeartRate(GoogleFitRequest request)
        {
            return Task.FromResult(new GoogleFitResponse());
        }
    }
}