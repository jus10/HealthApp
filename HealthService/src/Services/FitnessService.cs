using HealthService.Contexts;
using Shared.Contracts;
using Shared.Models;
using Shared.Models.Fitness;

namespace HealthService.Services
{
    public class FitnessService : IFitnessService
    {
        private readonly IFitnessContext _context;

        private readonly ILogger<FitnessService> _logger;

        public FitnessService(IFitnessContext context, ILogger<FitnessService> logger)
        {
            _context = context;
            _logger = logger;
        }

        public async Task<Empty> AddRoutine(Routine request)
        {
            _context.AddRoutine(request);
            return new Empty();
        }

        public async Task<Routine?> GetRoutineByDate(Routine request)
        {
            var res = _context.GetRoutineByDate(request);
            return res;
        }

        public async Task<ICollection<Routine>> GetRoutines(User user)
        {
            var res = _context.GetRoutines(user.Id);
            return res;
        }

        public async Task<Empty> UpdateActivities(Routine request)
        {
            _context.UpdateActivities(request);
            return new Empty();
        }
    }
}