using System.Data.Common;
using Google.Protobuf.WellKnownTypes;
using HealthService.Contexts;
using HealthService.Services;
using Microsoft.EntityFrameworkCore;
using ProtoBuf.Grpc.Server;
using ProtoBuf.Meta;

var builder = WebApplication.CreateBuilder(args);
System.AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);

var typeModel = RuntimeTypeModel.Default;
typeModel.Add(typeof(Int32Value), true);
typeModel[typeof(Int32Value)].AddField(1, nameof(Int32Value.Value));
typeModel.AutoAddMissingTypes = true;
// Additional configuration is required to successfully run gRPC on macOS.
// For instructions on how to configure Kestrel and gRPC clients on macOS, visit https://go.microsoft.com/fwlink/?linkid=2099682

// Register DBProviderFactory
DbProviderFactories.RegisterFactory("Npgsql", Npgsql.NpgsqlFactory.Instance);

builder.Services.AddDbContext<IApplicationContext, HealthService.Contexts.ApplicationContext>(options => options.UseNpgsql(builder.Configuration["Health:TestConnectionString"]));
builder.Services.AddDbContext<IAuthContext, HealthService.Contexts.ApplicationContext>(options => options.UseNpgsql(builder.Configuration["Health:TestConnectionString"]));
builder.Services.AddDbContext<IFitnessContext, HealthService.Contexts.ApplicationContext>(options => options.UseNpgsql(builder.Configuration["Health:TestConnectionString"]));
builder.Services.AddDbContext<IHealthContext, HealthService.Contexts.ApplicationContext>(options => options.UseNpgsql(builder.Configuration["Health:TestConnectionString"]));
builder.Services.AddCodeFirstGrpc();
builder.Services.AddGrpc();

var app = builder.Build();
app.MapGrpcService<AuthService>();
app.MapGrpcService<GoogleFitService>();
app.MapGrpcService<FitnessService>();
app.UseRouting();
app.UseEndpoints(endpoints =>
{
    if (app.Environment.IsDevelopment())
    {
        endpoints.MapGrpcReflectionService();
    }
});
// Configure the HTTP request pipeline.
app.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");
app.Run();