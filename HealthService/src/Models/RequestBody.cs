using System.Text.Json.Serialization;

namespace HealthService.Models
{
    public class AggregateBy
    {
        public string DataTypeName { get; set; }
        public string DataSourceId { get; set; }
    }

    public class BucketByTime
    {
        public int DurationMillis { get; set; }
    }

    public class RequestBody
    {
        [JsonPropertyName("aggregateBy")]
        public List<AggregateBy> AggregateBy { get; set; }

        [JsonPropertyName("bucketByTime")]
        public BucketByTime BucketByTime { get; set; }

        [JsonPropertyName("startTimeMillis")]
        public long StartTimeMillis { get; set; }

        [JsonPropertyName("endTimeMillis")]
        public long EndTimeMillis { get; set; }
    }
}