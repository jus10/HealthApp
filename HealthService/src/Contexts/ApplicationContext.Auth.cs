using HealthService.Utils;
using Microsoft.EntityFrameworkCore;
using Shared.Models;

namespace HealthService.Contexts
{
    public partial class ApplicationContext : DbContext, IApplicationContext, IAuthContext
    {
        public DbSet<Authentication> Authentication { get; set; }

        public void RegisterUser(User user, Authentication auth)
        {
            // Hash the password and add the salt
            var hashed = CryptoConverter.Encrypt(auth.Password);
            auth.Password = hashed.Password;
            auth.Salt = BitConverter.ToString(hashed.Salt);

            var ret = Users.Add(user);
            ret.Entity.Authentication = auth;
        }

        public void UpdateUserToken(UserAccessToken uat)
        {
            var auth = Authentication.Where(u => u.User.Id == uat.UserId).Single();
            if (auth.Scopes is null)
            {
                auth.Scopes = new();
            }
            auth.Scopes.Add(uat.Scope);
            auth.AccessToken = uat.Token.Token;
            auth.TokenType = uat.Token.Type;
            auth.RefreshToken = uat.Token.RefreshToken;
            auth.ExpiresIn = uat.Token.ExpiresIn;

            Authentication.Update(auth);
        }

        public AuthResponse VerifyUser(UserInfo userInfo)
        {
            var defaultResponse = new AuthResponse { UserId = -1, IsAuthenticated = false };
            var auth = Authentication.Where(a => a.User.Email.Equals(userInfo.Email))
            .Include(u => u.User)
            .FirstOrDefault();
            if (auth is null)
            {
                return defaultResponse;
            }

            var hashedPassword = auth.Password;
            if (!CryptoConverter.Encrypt(userInfo.Password, auth.Salt).Equals(hashedPassword))
            {
                return defaultResponse;
            }

            return new AuthResponse { UserId = auth.User.Id, IsAuthenticated = true };
        }
    }
}
