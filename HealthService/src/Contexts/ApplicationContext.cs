using Microsoft.EntityFrameworkCore;
using Shared.Models;
using Shared.Models.Fitness;

namespace HealthService.Contexts
{
    public partial class ApplicationContext : DbContext, IApplicationContext
    {
        public DbSet<User> Users { get; set; }

        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
        }

        protected ApplicationContext(DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}