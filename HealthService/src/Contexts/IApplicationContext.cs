using Microsoft.EntityFrameworkCore;
using Shared.Models;

namespace HealthService.Contexts
{
    public interface IApplicationContext
    {
        int SaveChanges();

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}