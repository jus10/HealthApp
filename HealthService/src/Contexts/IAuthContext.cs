using Shared.Models;

namespace HealthService.Contexts
{
    public interface IAuthContext : IApplicationContext
    {
        void RegisterUser(User userInfo, Authentication auth);

        AuthResponse VerifyUser(UserInfo info);

        void UpdateUserToken(UserAccessToken uat);
    }
}