using Shared.Models;
using Shared.Models.Fitness;

namespace HealthService.Contexts
{
    public interface IFitnessContext : IApplicationContext
    {
        /// <summary>
        /// Adds a new routine
        /// </summary>
        /// <param name="routine"></param>
        void AddRoutine(Routine routine);

        /// <summary>
        /// Gets all routines for the user
        /// </summary>
        /// <returns></returns>
        ICollection<Routine> GetRoutines(int userId);

        Routine GetRoutineByDate(Routine routine);

        void UpdateActivities(Routine routine);
    }
}