using System.Configuration;
using Microsoft.EntityFrameworkCore;
using Shared.Models;
using Shared.Models.Fitness;

namespace HealthService.Contexts
{
    public partial class ApplicationContext : IFitnessContext
    {
        public DbSet<Routine> Routines { get; set; }

        public DbSet<Activity> Activities { get; set; }

        public DbSet<Set> Sets { get; set; }


        public void AddRoutine(Routine request)
        {
            var user = Users.Where(u => u.Id.Equals(request.User.Id))
                .Include(r => r.Routines)
                .FirstOrDefault();

            if (user.Routines is null)
            {
                user.Routines = new List<Routine>();
            }

            request.DateCreated = DateTime.Now;
            request.DateUpdated = DateTime.Now;

            user.Routines.Add(request);
            SaveChanges();
        }

        public ICollection<Routine> GetRoutines(int userId)
        {
            var routines = Routines.Where(r => r.User.Id.Equals(userId))
                .Include(a => a.Activities)
                .DefaultIfEmpty()
                .ToList();

            return routines;
        }

        public Routine GetRoutineByDate(Routine request)
        {
            var day = request.DatePerformed.ToString("ddd").ToLower();

            // Get the default routine for the day
            var routines = Routines.Where(r => r.Days.Contains(day))
                .Include(a => a.Activities)
                .ThenInclude(s => s.Sets.Where(c => c.Routine.DatePerformed.Equals(request.DatePerformed)));

            if (routines is null)
            {
                return null;
            }

            // If the routine has no history data generate a new template
            var routine = routines.Where(r => r.DatePerformed.Equals(request.DatePerformed));

            if (routine.Count() == 0)
            {
                var defaultRoutine = routines
                    .Include(a => a.Activities)
                    .ThenInclude(s => s.Sets)
                    .FirstOrDefault();


                var r = BuildDefaultActivities(defaultRoutine, request);
                Routines.Add(r);
                SaveChanges();

                return r;
            }
            return routine.First();
        }

        public void UpdateActivities(Routine routine)
        {
            var r = Routines.Where(r => r.Label.Equals(routine.Label) && r.DatePerformed.Equals(routine.DatePerformed))
                .Include(u => u.User)
                .Include(a => a.Activities)
                .ThenInclude(s => s.Sets.Where(c => c.Routine.DatePerformed.Equals(routine.DatePerformed)))
                .SingleOrDefault();

            if (r is null)
            {
                var user = Users.Where(u => u.Id.Equals(routine.User.Id))
                    .Include(u => u.Routines)
                    .Single();

                r = new Routine
                {
                    Activities = routine.Activities,
                    Label = routine.Label,
                    DateCreated = DateTime.Now,
                    DatePerformed = routine.DatePerformed,
                    DateUpdated = DateTime.Now,
                    Days = routine.Days,
                    User = user
                };
                user.Routines.Add(r);
            }
            else
            {
                foreach (var activity in routine.Activities)
                {
                    var activityToUpdate = r.Activities.Where(a => a.Id.Equals(activity.Id)).Single();
                    var newSets = new List<Set>();

                    foreach (var set in activity.Sets)
                    {
                        var setToUpdate = activityToUpdate.Sets
                            .Where(s => s.SetId.Equals(set.SetId) && activityToUpdate.Label.Equals(activity.Label))
                            .FirstOrDefault();

                        if (setToUpdate is null)
                        {
                            var newSet = new Set
                            {
                                Repetitions = set.Repetitions,
                                Weight = set.Weight,
                                DateCreated = DateTime.Now,
                                DateUpdated = DateTime.Now,
                                SetId = set.SetId
                            };
                            activityToUpdate.Sets.Add(newSet);

                        }
                        else
                        {
                            setToUpdate.Repetitions = set.Repetitions;
                            setToUpdate.Weight = set.Weight;
                        }
                    }
                }
            }
            r.DateUpdated = DateTime.Now;
            r.DatePerformed = routine.DatePerformed;
            SaveChanges();
        }

        private Routine BuildDefaultActivities(Routine routine, Routine request)
        {
            var newRoutine = new Routine();
            newRoutine.Activities = new List<Activity>();

            foreach (var activity in routine.Activities)
            {
                var sets = new List<Set>();
                for (var i = 0; i < activity.TotalSets; i++)
                {
                    activity.Sets.Add(new Set
                    {
                        SetId = i + 1,
                        Repetitions = 0,
                        Weight = 0,
                        Routine = newRoutine,
                        Activity = activity
                    });

                }
                newRoutine.Activities.Add(activity);
            }

            newRoutine.Label = routine.Label;
            newRoutine.User = Users.Where(u => u.Id.Equals(request.User.Id)).Single();
            newRoutine.DateCreated = DateTime.Now;
            newRoutine.DateUpdated = DateTime.Now;
            newRoutine.DatePerformed = request.DatePerformed;
            newRoutine.Days = routine.Days;

            return newRoutine;
        }
    }
}